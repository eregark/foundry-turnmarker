# 2.6.2
- Moved global settings into their own window
- Add support for localization (translators desired!)

# 2.6.1
- Now properly integrates with Combat Utility Belt's 'Hide NPC Names' option
- Fix for multiple turn change messages when a combatant is removed from combat


# 2.6.0
- Now supports hex grids properly
- New feature: Setting to announce turn changes with a chat message

# 2.5.1
- Fix for error thrown when removing the last combatant from combat if combat has not started

# 2.5
- Updated for new tile structure in 0.5.6+

# 2.4.2
- Last release for 0.5.5-

# 2.4.1
- Ensure compatibility with 0.5.6

# 2.4
- Fix for marker misbehaving when token vision is disabled for a scene
- Fix for marker being visible when tokens are hidden

# 2.3
- Marker should now be hidden when the active combatant is hidden

# 2.2
- Fix for multiple markers being placed, but not updated when more than one GM is logged in
- Fix for error when changing image curing combat while player is connected

# 2.1
- Fix error when trying to change the marker image when no combat is active
- Fix error when moving a token outside of combat

# 2.0
- Change animation to be SIGNIFICANTLY smoother by using PIXI animations
- Remove "Animation Degrees" setting as it is no longer needed
- Marker should no longer hide behind other tiles on the canvas
- Each user can now define their own animation settings

# 1.0
- Initial Release